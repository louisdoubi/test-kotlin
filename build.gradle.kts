plugins {
    kotlin("jvm") version "1.9.0"
    id("com.google.devtools.ksp") version("1.9.0-1.0.11")
    application
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.ktor:ktor-server-core:2.3.2")
    implementation("io.ktor:ktor-server-netty:2.3.2")
    implementation("io.ktor:ktor-server-content-negotiation:2.3.2")
    implementation("io.ktor:ktor-serialization-kotlinx-json:2.3.2")
    implementation("io.insert-koin:koin-core:3.4.2")
    implementation("io.insert-koin:koin-annotations:1.2.2")
    implementation("io.insert-koin:koin-ktor:3.4.1")
    implementation("io.insert-koin:koin-logger-slf4j:3.4.1")
    testImplementation("org.jetbrains.kotlin:kotlin-test:1.8.20-RC")
}

tasks.test {
    useJUnitPlatform()
}

application {
    mainClass.set("MainKt")
}
import org.koin.core.annotation.Single

@Single
class DuplicateService {

    private fun List<Int>.removeNumbersAppearingMoreThanNTimes(n: Int): List<Int> {
        val occurrences = groupBy { it }.mapValues { it.value.size }
        return filter { (occurrences[it] ?: 0) <= n }
    }

    fun removeNumbers(input: List<Int>, n: Int): Output {
        return Output(input.removeNumbersAppearingMoreThanNTimes(n));
    }

}
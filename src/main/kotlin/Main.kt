import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.application.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.routing.*
import org.koin.core.component.get
import org.koin.core.context.startKoin
import org.koin.ktor.ext.inject
import org.koin.logger.slf4jLogger


fun Application.main() {
    install(ContentNegotiation) {
        json()
    }

    startKoin {
        slf4jLogger()
    }
    routing {

        route("/api") {
            val controller: DuplicateController by inject()
            controller.run{ duplicateRoutes() }
        }
    }
}

fun main() {
    EngineMain.main(arrayOf("-config=application.conf"))
}
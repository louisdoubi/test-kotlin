import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*
import org.koin.core.annotation.ComponentScan
import org.koin.core.annotation.Single
import org.koin.core.component.KoinComponent

@Single
@ComponentScan
 class DuplicateController(private val duplicateService: DuplicateService): KoinComponent  {
     fun Route.duplicateRoutes() {
    get("remove-duplicated") {
        val n = call.parameters.getOrFail<Int>("n").toInt()
        val input = call.parameters.getOrFail<List<Int>>("input").toList()
        call.respond(duplicateService.removeNumbers(input, n))
    }
  }
}
